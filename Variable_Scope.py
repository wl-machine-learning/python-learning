#reference links:
#https://www.guru99.com/variables-in-python.html
#http://codinglabs.in/#/workspace/7/tracks/232/view/details

a=10; # 'a' and 'b' are global variables
b=20;

def myfunction(a):
    c=30    #'c' local variable scope applied inside myfunction only, after that it will get destory
    print(c+a)

print(a) #'a' still live because of global scope.

#we can delete the defined variables using 'del' keyword
del a
print(a)