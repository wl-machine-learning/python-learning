#defining a tuple
person1 = ("Gopikrishna", 26, "Software engineer")
#printing a tuple
print(person1)
#printing the first element in tuple
print(person1[0])
#printing the first element in tuple
print(person1[0:1])
#print(person1[0:2]) prints the elements index position 0 and 1. Outbound is exclusive
print(person1[0:2])
print(person1[0:3])

#tuple packing
company =("Wavelabs tehnologies","Hyderabad",296)

#tuple unpacking
(company_name, location, no_of_employees)=company
print(company_name)
print(location)
print(no_of_employees)

#tuple Comparing

village1=(2000, "Dabbakupalli")
village2=(1500,"PentalaGudem")
#While comparing the tuple, it uses first element in tuple to compare.
#If first element in the tuple same both then it will compare with 2nd element in tuple
if(village1>village2):
    print("Dabbakupalli has more population")
else:
    print("Pentalagudem has more population")

#Built-in functions with Tuple
#all(), any(), enumerate(), max(), min(), sorted(), len(), tuple() etc..
print(len(village2))
elements=(1,2,10,4,5)
print(max(elements))
