#A Python Array is a common type of data structure,
# where in all elements must be of the same data type.
#Syntax to create the array arrayName = array.array(type code for data type, [array,items])

# reference material : https://www.guru99.com/python-arrays.html#1

import array as myarray

#Here 'd' represents float data type in python,
# However In other programming laungaes like 'c' and 'java' it represents the interger
#refer https://www.guru99.com/python-arrays.html#1 for datatype table
randomFloats = myarray.array('d', [2.3,3.4,5.5])
print(randomFloats[1])
print(len(randomFloats))
randomFloats.insert(3,6.2)
print(len(randomFloats))

