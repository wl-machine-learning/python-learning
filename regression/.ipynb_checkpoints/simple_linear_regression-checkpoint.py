import pandas as pandas
import numpy as np
data = pandas.read_csv("salary.csv").values
feature_exp = data[:, 0]
outcome_sal = data[:, 1]
'''
print(feature_exp)
print("************")
print(outcome_sal)
'''
from sklearn.model_selection import train_test_split

feature_exp_train, feature_exp_test, outcome_sal_train, outcome_sal_test = train_test_split(feature_exp, outcome_sal, test_size=1/3, random_state=0)

print(feature_exp_train)
print("*****************")
'''
If we have single feature, we can't send 1d array for LinearRegression model
We should send the it as 2nd array. To convert it to 2d array use below code
'''
feature_exp_train = feature_exp_train.reshape(-1,1)
print(feature_exp_train)
from sklearn.linear_model import LinearRegression
regressor = LinearRegression()
#Fitting Simple Linear regression to the training set
regressor = regressor.fit(feature_exp_train, outcome_sal_train)
#print(regressor)
