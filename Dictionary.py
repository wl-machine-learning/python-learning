#reference : https://www.guru99.com/python-dictionary-beginners-tutorial.html#2

#dictionaries declaration
employeesDesignationDictionary ={
    "Gopikrishna":"Software engineer",
    "Sekharkari":"Senior Software engineer",
    "Naresh kumar":"Junior software Engineer",
    "Krishnaveni":"Software Engineer"
}

persons = {
    "Gopikrishna":26,
    "Haripriya":25,
    "Naresh kumar":26,
    "Krishnaveni":29
}
boys = {
    "Gopikrishna":26,
     "Naresh kumar":26
}
#copying boys dictionary to a new dictionary
x = boys.copy();
print(x)
#printing age of haripriya before updating
print(persons.get("Haripriya"))
#updating the dictionary
persons.update({"Haripriya":24})
#printing the dictionary value using key
print(persons.get("Haripriya"))

#using items method on dictionary
print("Persons Name: %s" % list(persons.items()))

#iterating elements over the for loop with dictionaries and adding if-else conditons
for key in list(persons.keys()):
     if key in list(boys.keys()):
         print(str(key+ " is a boy"))
     else:
         print(str(key+" is a girl"))

#sorting the dictionary based on the names (key)
sortedNames = list(persons.keys())
sortedNames.sort()
#printing in sorted order
for name in (sortedNames):
    print("name is " + name+" age is ", str(persons.get(name)))

#printing type of variables
print("variable Type: %s" % type(persons))
print("variable Type: %s" % type(sortedNames))

#Merge two dictionaries using update() method
stateAndCaptials1 = {"AP":"Vizag, Amaravathi and Kurnool", "TS":"Hyderabad"}
stateAndCaptials2 = {"Tamilnadu":"Chennai", "Maharastra":"Mumbai"}
#merging two dictionaries using syntax1
stateAndCaptials1.update(stateAndCaptials2)
print("After merging using 1st syntax %s ", stateAndCaptials1)
#merging two dictionaries using syntax2 from python 3.5
stateAndCaptials2= {**stateAndCaptials1, **stateAndCaptials2}
print("After merging using 2nd syntax, %s", stateAndCaptials2)

#checking a key exist in Dictionary or not
print("Gopikrishna" in persons)
print("Krishnaveni" in persons)
print("Sekhar" in persons)

