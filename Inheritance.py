#Parent class
class Vechile():
    def start(self):
        print("Startig the vechile")

    def stop(self):
        print("Stopping vechile")
"""Java Equivalent syntax 
       class Yamaha extends Vechile {
          //
       }"""
class Yamaha(Vechile):
    def drive(self):
        print("Driving the Yamamaha vechile")

class RoyalEnfield(Vechile):
    def drive(self):
        print("Driving the RoyalEnfield Bike")


def main():
   royalBike =  RoyalEnfield()
   royalBike.start()
   royalBike.drive()
   royalBike.stop()

if __name__ == '__main__':
    main()
