import pandas as pandas
import numpy as np
import matplotlib as mp
'''
   There are changes in Imputer package and class locations, 
   refer this stack over flow link: 
   <a> https://stackoverflow.com/questions/59439096/importerror-cannnot-import-name-imputer-from-sklearn-preprocessing </a>
'''
from sklearn.impute import SimpleImputer as simpleImputer
#This will read the csv values, returns the data
dataset = pandas.read_csv("Data.csv")

#read the only independent columns and skip last dependent column from data.csv file
independentValues = dataset.iloc[:,:-1].values
#print(independentValues)

'''
   Example data: 
 [['France' 44.0 72000.0]
 ['Spain' 27.0 48000.0]
 ['Germany' 30.0 54000.0]
 ['Spain' 38.0 61000.0]
 ['Germany' 40.0 nan]
 ['France' 35.0 58000.0]
 ['Spain' nan 52000.0]
 ['France' 48.0 79000.0]
 ['Germany' 50.0 83000.0]
 ['France' 37.0 67000.0]]
If you notice above, In second column one Nan an in 3rd clumn one Nan values are there
They are data missing places, We need to fill that data with "mean". To make it very easy
We can use SimpleImputer class in python
'''
simpleImputer = simpleImputer(missing_values=np.nan, strategy="median")
'''
In the above statement, we used np.nan. directly using of "Nan" (or) "nan" throwing error
Older imputer class directly worked with "nan" (or) "Nan"
'''
simpleImputer = simpleImputer.fit(independentValues[:,1:3])
independentValues[:,1:3] = simpleImputer.transform(independentValues[:,1:3])
print(independentValues)

'''
new values will be
[['France' 44.0 72000.0]
 ['Spain' 27.0 48000.0]
 ['Germany' 30.0 54000.0]
 ['Spain' 38.0 61000.0]
 ['Germany' 40.0 61000.0]
 ['France' 35.0 58000.0]
 ['Spain' 38.0 52000.0]
 ['France' 48.0 79000.0]
 ['Germany' 50.0 83000.0]
 ['France' 37.0 67000.0]]
'''


#categorical the data
from sklearn.preprocessing import LabelEncoder
#replace the Country names with numbers
labelEncoder = LabelEncoder()
independentValues[:,0] = labelEncoder.fit_transform(independentValues[:,0])
#print(independentValues)
'''
Above statement will print like this
[[0 44.0 72000.0]
 [2 27.0 48000.0]
 [1 30.0 54000.0]
 [2 38.0 61000.0]
 [1 40.0 61000.0]
 [0 35.0 58000.0]
 [2 38.0 52000.0]
 [0 48.0 79000.0]
 [1 50.0 83000.0]
 [0 37.0 67000.0]]
It gives one value to every country starting from zero. There is a problem here
In equations people may compare this values but they are not real values just labels
To solve this problem we will use below approach 
'''

from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
#columnTransformer = ColumnTransformer("Country", OneHotEncoder(), [0])
columnTransformer = ColumnTransformer([("Country", OneHotEncoder(), [0])], remainder = 'passthrough')
independentValues = columnTransformer.fit_transform(independentValues)
print(independentValues)
'''
output will be as follows: It will generate as matrix for each value
[[1.0 0.0 0.0 44.0 72000.0]
 [0.0 0.0 1.0 27.0 48000.0]
 [0.0 1.0 0.0 30.0 54000.0]
 [0.0 0.0 1.0 38.0 61000.0]
 [0.0 1.0 0.0 40.0 61000.0]
 [1.0 0.0 0.0 35.0 58000.0]
 [0.0 0.0 1.0 38.0 52000.0]
 [1.0 0.0 0.0 48.0 79000.0]
 [0.0 1.0 0.0 50.0 83000.0]
 [1.0 0.0 0.0 37.0 67000.0]]
 
 reference links for quick recall : 
 https://www.geeksforgeeks.org/ml-one-hot-encoding-of-datasets-in-python/
'''