'''

If you notice data in Data.csv file, There are two columns
1) Age
2) Salary

When we are finding Euclidean distance between these two points, Salary has dominance
So in Machine learning computation Age almost has no effect, To solve this problem
we use 'Standardisation' and 'Normalisation', See the image in this directory for formula
'''