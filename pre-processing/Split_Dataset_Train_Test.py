import pandas as pandas
import numpy as np
from sklearn.impute import SimpleImputer
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sklearn.compose import ColumnTransformer
#returns the numpy array
data = pandas.read_csv("Data.csv").values
#print(data)
#creating SimpleImputer object for
simpleImputer = SimpleImputer(missing_values=np.nan, strategy="mean")
print(type (data))
#print(data[:, 1:3])
simpleImputer = simpleImputer.fit(data[:, 1:3])
data[:,1:3] = simpleImputer.fit_transform(data[:, 1:3])
#print("****** AFTER REPLACING VALUES ************")
#print(data[:, 1:3])

#Take input columns values to one variable and predictble column in other variable
person_features = data[:, 0:3]
purchase_outcome = data[:,3]
#print(person_features)
print("*****************")
#print(purchase_outcome)
#Labelling the data
labelEncoder = LabelEncoder()
person_features[:, 0] = labelEncoder.fit_transform(person_features[:, 0])
columnTransformer = ColumnTransformer([("Country", OneHotEncoder(), [0])], remainder = 'passthrough')
person_features = columnTransformer.fit_transform(person_features)
#Labelling the purchaseOutCome data
purchase_outcome = labelEncoder.fit_transform(purchase_outcome)
#print(person_features)

#Split the data into the Training and Test
'''
In this example person_properties are information of a person and outcome is purchased yes (or) no
From CSV file we split the data into two parts. One is features of person and 2nd is action he performed
'''
#Split the data into two parts 1) Training (2) Test dataset

from sklearn.model_selection import train_test_split

person_features_train, person_features_test, purchase_outcome_train, purchase_outcome_test  = train_test_split(person_features, purchase_outcome, test_size=0.2, random_state=0)
'''
print(person_features_train)
print(purchase_outcome_train)
print("********")
print(person_features_test)
print(purchase_outcome_test) '''
'''
If we notice salary and age both features are not on same scale, So age has less significance on model training
To build the accurate model all features of person should be on same scale. To do that we can use standardization and normalization
To see the forumula check the image in this directory.

Question: What is deciding factor between standardization and normalization while converting the data to same scale.

in below snippet we are using standardization
'''

from sklearn.preprocessing import StandardScaler
standardScalar = StandardScaler()
#Below line converts all the features to the same scale
person_features_train = standardScalar.fit_transform(person_features_train)
#For test data no need to fit, Need to find out the reason.
person_features_test = standardScalar.transform(person_features_test)
print(person_features_train)