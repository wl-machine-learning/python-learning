import numpy as np
import matplotlib.pyplot as  plt
import pandas as pd
#reads the file data into a variable
dataset = pd.read_csv("Data.csv")
#printing the file data
#print(dataset)

#reading required rows and columns examples from dataset variable
''' 
 1) First -1  removes the last row from result
 2) Second -1 removes the last column from result
 3) dataset.iloc[:,:] gives all rows and columns
 4) dataset.iloc[:-2, :-2] removes the last two rows and last two columns from result
'''
#allRows = dataset.iloc[:, :-1]
independentVariables_X = dataset.iloc[:, :-1].values
''''
    1) If a negitive number is specified they will be removed from result. iloc[:-1,:-1]
    2) If a positive number is specified they will included in result. iloc[:,:1]
'''
#.values will be converts result into a array
dependentVariables_Y = dataset.iloc[:,3].values
print(dependentVariables_Y)

