print("Hello world")
#below syntax prints the name "gopi" 4 times
print( 4 * "gopi\n")

#By default, python's print() function ends with a newline.
# This function comes with a parameter called 'end.'
# The default value of this parameter is '\n,' i.e., the new line character.
# You can end a print statement with any character or string using this parameter.
# This is available in only in Python 3+
print("Gopi", end=" krishna\n")