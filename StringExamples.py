name = "Gopikrishna"
#printing the sub string
print(name[0:4])
#converting to upper case
print(name.upper())
#converting to lower case
print(name.lower())
#splitting the String and returns the elements as list
tokens = name.split("i")
#print the type of tokens
print(type(tokens))
#returns the avalible methods in list
#help(tokens)

#printing the length of String
print(len(name))
#printing the number of times a character repetead in a string
print(name.count("i"))
#printing the number of times a character repeated in the boundaries
print(name.count("i",0,5))
#finding the strting index position of substring
name="tomato"
print(name.find("to"))
#finding the last index position of substring
print(name.rfind("to"))