#creating lamda functions.
#Lamdas are anonymous functions
"""
In Python, lambda expressions (or lambda forms) are utilized to construct anonymous functions.
To do so, you will use the lambda keyword (just as you use def to define normal functions).
Every anonymous function you define in Python will have 3 essential parts:

The lambda keyword.
The parameters (or bound variables), and
The function body.
A lambda function can have any number of parameters, but the function body can only contain one expression. Moreover, a lambda is written in a single line of code and can also be invoked immediately"""

adder = lambda x, y: x + y
print(adder(2,3))

names = ["Gopi", "Krishna", "Srinu", "Lakshmi"]

#find the names which has alphabet 'a'.
#filter uses to filter the elements based on condition using lambda function
filteredNames = filter(lambda name : name.count("a")>0, names)
print(list(filteredNames))

#map : map is the function useful to apply a lambda function to all elements in a list
#In below example concating -WL at the end to every name
resultMap = map(lambda name : (name + "-WL"), names)
print(list(resultMap))

"""
    filter --> elements the elements from list based on condition
    map    --> Apply the data transformation logic to every element in list
    reduce --> it will be used as accumlator, It will stores the computation results as
               per lamda function and cummilatively returns single value 
"""
import Person
p1 = Person.Person("Gopikrishna", 26)
p2 = Person.Person("Brunda",22)
personList = [p1,p2]
#Find the total ages of all persons
from functools import reduce
totalAge = reduce(lambda p1,x : x+p1.getAge(), personList)
print(totalAge)
